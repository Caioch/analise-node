var express = require('express');
var path = require('path');
var app = express();



var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
    mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
    mongoURLLabel = "";

app.set('port', port);

app.use(express.static(path.join(__dirname, 'public')));
app.listen(port, ip, function() {
     console.log('Magic happens on port ' + port);
});


module.exports = app;